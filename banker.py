import random

class Banker:
  """
  Represents the banker who manages the deck of diamonds and selects cards for auction.
  """
  def __init__(self, deck):
    self.deck = deck

  def draw_diamond(self):
    """
    Randomly selects a diamond card from the deck.
    """
    if len(self.deck) == 0:
      return None  # No diamonds left
    diamond_index = random.randint(0, len(self.deck) - 1)
    return self.deck.pop(diamond_index)