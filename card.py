class Card:
  """
  Represents a playing card with suit, rank, and point value.
  """
  def __init__(self, suit, rank):
    self.suit = suit
    self.rank = rank
    self.point_value = rank if rank <= 10 else 10 + (rank - 10)  # Assign point values for face cards
