from card import Card
from banker import Banker
from player import Player
from player import AIPlayer
from player import AIPlayer2

import random

class Game:
  """
  Represents a game instance of Diamond Auction.
  """
  def __init__(self, num_players):
    self.players = []
    self.banker = None
    self.num_players = num_players
    self.setup_game()



  def setup_game(self):
    suits = ["Hearts", "Clubs", "Spades"]  # Adjust for 3 players if needed
    self.banker = Banker(self.create_diamond_deck())
    remaining_suits = suits.copy()

    for i in range(self.num_players):
        player_suit = random.choice(remaining_suits)  # Randomly choose a remaining suit
        remaining_suits.remove(player_suit)  # Remove chosen suit for next player

        if i == self.num_players - 1:  # Check for last player (AI player)
           aggressiveness = float(input("Enter desired aggressiveness level for AI player (0 - cautious, 1 - aggressive): "))
           player = AIPlayer(f"AI Player", aggressiveness)
        elif i == self.num_players - 2:
           player = AIPlayer2(f"AI Player2")
        else:
            player = Player(f"Player {i+1}")
        player.suit = player_suit  # Assign chosen suit to the player
        self.players.append(player)

    # Deal all cards of the assigned suit to each player
    for player in self.players:
        for rank in range(2, 15):
            player.hand.append(Card(player.suit, rank))

  def create_diamond_deck(self):
    """
    Creates a deck of 13 diamond cards with point values.
    """
    deck = []
    for rank in range(2, 15):
      deck.append(Card("Diamonds", rank))
    random.shuffle(deck)
    return deck

  def deal_cards(self, suit):
    """
    Deals all cards of a specific suit to a player.
    """
    deck = []
    for rank in range(2, 15):
      deck.append(Card(suit, rank))
    return deck

  def play_game(self):
    """
    Executes the main game loop.
    """
    while True:
      # Auction loop
      diamond = self.banker.draw_diamond()
      if diamond is None:
        break  # All diamonds auctioned, end game
      self.play_auction_round(diamond)

    # End Game
    print(f"\nGame Over! Scores:")
    for player in self.players:
      print(f"{player.name}: {player.score}")
    winner = max(self.players, key=lambda p: p.score)
    print(f"\nWinner: {winner.name}")

  def play_auction_round(self, diamond):
    """
    Conducts a single round of auction for a revealed diamond.
    """
    print(f"\nAuctioning Diamond: {diamond.point_value}")


    bids = []
    for player in self.players:
        bid = player.get_player_bid(self, diamond)
        if bid:
            print(f"{player.name} bids {bid.rank}{bid.suit[0]}")
        else:
            print(f"{player.name} passes")

        bids.append(bid)

    # Reveal bids and determine winner(s)
    winner_indices = self.get_winners(bids)
    for winner_index in winner_indices:
      winner = self.players[winner_index]
      winner.score += diamond.point_value
      print(f"{winner.name} wins the round with {bids[winner_index].rank} and receives a Diamond {diamond.point_value}!")

    print("\nCurrent Scores:")
    for player in self.players:
        print(f"{player.name}: {player.score}")

    # Discard used cards
    for player, bid in zip(self.players, bids):
      player.hand.remove(bid)


  def get_winners(self, bids):
    highest_rank = None
    winning_indices = []
    for i, bid in enumerate(bids):
        if bid is not None and (highest_rank is None or bid.rank > highest_rank):
            highest_rank = bid.rank
            winning_indices = [i]  # Reset winning indices for highest rank
        elif bid is not None and bid.rank == highest_rank:
            winning_indices.append(i)  # Add indices for tied bids
    return winning_indices

# Example usage
game = Game(3)  # Change to 3 for 3 players
game.play_game()