import random
class Player:
    """
    Represents a player in the game with a hand of cards, score, and assigned suit.
    """
    def __init__(self, name):
        self.name = name
        self.hand = []
        self.score = 0
        self.suit = None  # Assigned during setup

    def get_player_bid(self, game, diamond):
        """
        Allows a human player to choose a card from their hand as a bid.
        """
        while True:
            print(f"\n{self.name}'s Hand:")
            for card in self.hand:
                print(f"{card.rank}{card.suit[0]}", end=" ")  # Display cards in hand

            choice = input(f"\nChoose a card to bid (e.g., 2, 3, J, A): ").strip().upper()
            rank_names = {"T": 10, "J": 11, "Q": 12, "K": 13, "A": 14}

            if choice in rank_names:
                rank = rank_names[choice]
            elif choice.isdigit() and int(choice) in range(2, 15):
                rank = int(choice)
            else:
                print("Invalid choice. Please enter a valid card (e.g., 2, 3, J, A).")
                continue

            for card in self.hand:
                if card.rank == rank:
                    return card

            print(f"You don't have a {choice} in your hand. Please choose a valid card.")

        return None  # Default return if no valid bid is made

class AIPlayer(Player):
    """
    Represents an AI player with an automated bidding strategy.
    """
    def __init__(self, name, aggressiveness):
        super().__init__(name)
        self.aggressiveness = aggressiveness  # 0 (cautious) to 1 (aggressive)

    def get_player_bid(self, game, diamond):
        """
        Implements an automated bidding strategy for the AI player based on aggressiveness.
        """
        highest_card = max(card.rank for card in self.hand) if self.hand else 0

        if self.aggressiveness > 0.5 and diamond.rank > highest_card:
            return None  # AI is cautious and doesn't bid if diamond is higher than highest card
        else:
            # Bid the highest card in hand that can match or exceed the diamond rank
            for card in sorted(self.hand, key=lambda c: c.rank, reverse=True):
                if card.rank >= diamond.rank:
                    return card

        # If no suitable bid, return random card from hand
        return random.choice(self.hand) if self.hand else None

class AIPlayer2(Player):
    """
    Represents another AI player with a specific bidding strategy.
    """
    
    def get_player_bid(self, game, diamond):
        """
        Implements an automated bidding strategy for AI Player 2.
        """
        # Sort computer's deck based on card values
        sorted_computer_deck = sorted(self.hand, key=lambda card: card.rank)

        # Find the lowest valid card that can beat or match the revealed diamond card
        for card in sorted_computer_deck:
            if card.rank >= diamond.rank:
                return card
        
        # If no valid card can beat or match the revealed diamond, choose the lowest card
        return sorted_computer_deck[0] if sorted_computer_deck else None
